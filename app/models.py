from django.db import models


class DeviceModel(models.Model):
    model_name = models.CharField(max_length=250)


class Color(models.Model):
    hex_value = models.CharField(max_length=30)


class Product(models.Model):
    class Meta:
        abstract = True

    name = models.CharField(max_length=250)
    device_model = models.ForeignKey(DeviceModel, on_delete=models.CASCADE)
    color = models.ForeignKey(Color, on_delete=models.CASCADE)
    price = models.FloatField()
    image = models.ImageField(default=None)


class Category(models.Model):
    category_name = models.CharField(max_length=250)


class Material(models.Model):
    material_name = models.CharField(max_length=250)


class Case(Product):
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    device_model = models.ForeignKey(DeviceModel, on_delete=models.CASCADE)
    material = models.ForeignKey(Material, on_delete=models.CASCADE)


class Charge(Product):
    length = models.FloatField()  # meters


class Filler(models.Model):
    filler_name = models.CharField(max_length=250)


class Glass(Product):
    filler = models.ForeignKey(Filler, on_delete=models.CASCADE)


class BaseUser(models.Model):
    class Meta:
        abstract = True

    first_name = models.CharField(max_length=250)
    nickname = models.CharField(max_length=250)
    email = models.EmailField()
    password = models.CharField(max_length=250)


class Operator(BaseUser):
    second_name = models.CharField(max_length=50)
    patronymic = models.CharField(max_length=50)
    birth_date = models.DateField(auto_now_add=True)


class User(BaseUser):
    phone_number = models.CharField(max_length=15)


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    date = models.DateField(auto_now_add=True)
    delivery_address = models.CharField(max_length=250)
