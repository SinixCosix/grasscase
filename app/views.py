from django.shortcuts import render


def index_page(request):
    context = dict()
    context["cum"] = 'CUM CUM'
    context['page_name'] = 'Главная страница'

    return render(request, 'index.html', context)
